<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleDevice\HasDevice;

class UserModel extends Model
{
    use SoftDeletes, HasDevice;

    protected $table = 'simple_device_test_users';
}
