<?php

use Tests\UserModel;
use Orchestra\Testbench\TestCase;

class SimpleDeviceTest extends TestCase
{
    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testing');
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleDevice\SimpleDeviceServiceProvider::class,
        ];
    }

    public function init()
    {
        $user = (new UserModel([]));
        $user->save();

        return $user;
    }

    public function test()
    {
        $user = $this->init();

        $device = $user->addNewDevice([
            'device_uuid' => '12341234',
            'app_version' => 1,
            'ip_address' => '127.0.0.1'
        ]);

        $this->assertSame($device->device_uuid, '12341234');
        $this->assertSame($device->app_version, 1);
        $this->assertInstanceOf(UserModel::class, $device->user);

       try {
        $device = $user->addNewDevice([
            'device_uuid' => '12341234',
            'app_version' => 1,
            'ip_address' => '127.0.0.1'
        ]);
        $this->assertTrue(false);
       } catch (Throwable $e) {
           $this->assertTrue(true);
       }
    }
}
