<?php

namespace Yeknava\SimpleDevice;

use Illuminate\Database\Eloquent\Model;

class SimpleDevice extends Model
{
    protected $fillable = [
        'device_uuid',
        'device_type',
        'device_os',
        'device_os_version',
        'device_manufacturer_model',
        'device_screen_width',
        'device_screen_height',
        'app_version',
        'app_build_version',
        'ip_address',
        'notification_token',
        'user_token',
        'extra',
    ];

    public function user()
    {
        return $this->morphTo();
    }
}
