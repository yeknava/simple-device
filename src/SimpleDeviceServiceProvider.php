<?php

namespace Yeknava\SimpleDevice;

use Illuminate\Support\ServiceProvider;

class SimpleDeviceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            dirname(__DIR__, 1) . '/migrations/' =>
                database_path('migrations'),
        ], 'simple-device');
    }
}
