<?php

namespace Yeknava\SimpleDevice;

use Illuminate\Support\Facades\Validator;

trait HasDevice
{
    public function devices() {
        return $this->morphMany(SimpleDevice::class, 'user');
    }

    public function addNewDevice(array $data) : SimpleDevice {
        Validator::make($data, [
            'device_uuid' => 'required|unique:simple_devices',
            'device_type' => 'nullable|string|max:255',
            'device_os' => 'nullable|string|max:255',
            'device_os_version' => 'nullable|string|max:255',
            'device_brand' => 'nullable|string|max:255',
            'device_manufacturer_model' => 'nullable|string|max:255',
            'device_screen_width' => 'nullable|integer',
            'device_screen_height' => 'nullable|integer',
            'app_version' => 'nullable|numeric',
            'app_build_version' => 'nullable|numeric',
            'ip_address' => 'required|string|max:40',
            'notification_token' => 'sometimes|required|string',
            'user_token' => 'sometimes|required|string',
            'extra' => 'nullable|json',
        ]);
        $device = new SimpleDevice($data);
        $this->devices()->save($device);

        return $device;
    }
}
