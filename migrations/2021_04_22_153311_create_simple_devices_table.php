<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_devices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('user');
            $table->string('device_uuid')->unique();
            $table->string('device_type')->nullable();
            $table->string('device_os')->nullable();
            $table->string('device_os_version')->nullable();
            $table->string('device_brand')->nullable();
            $table->string('device_manufacturer_model')->nullable();
            $table->string('device_screen_width')->nullable();
            $table->string('device_screen_height')->nullable();
            $table->string('app_version', 20)->nullable();
            $table->string('app_build_version', 20)->nullable();
            $table->string('ip_address', 40);
            $table->text('notification_token')->nullable();
            $table->text('user_token')->nullable();
            $table->jsonb('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_devices');
    }
}
