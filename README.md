# Laravel Simple Device Package

Laravel Simple Device Package lets you handle store user's device data in database.

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple device package.

```bash
composer require yeknava/simple-device
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
